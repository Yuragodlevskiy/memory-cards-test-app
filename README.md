## Memory Cards Game 

### Task description:

- The screen should show 16 cards , on view 4 X 4.
- Each two cards have the same number.
- The numbers are from 1 to 8.
- Each card has two sides:
  - Front side:  the number.
  - Back side: image/color (same on all cards).
- Init state, all the cards showing the back side in random order.
- When a user clicks on a card, the card should flip to the number side.
- If the user clicks on two cards that have the same number, they will **not** flip back. Otherwise, both of the cards will flip back after 2 seconds, at this time the other cards aren't clickable.
- The game will end when all the cards are showing the numbers side.
- You should make a flip animation effect on the cards, and not just change the view from image to number and back.

### The quality of the task will be checked according to:
- Quality of the written code.
- Algorithm of the game.
- Quality of design (css).


